# Sebák Petra - Számítógépi grafika beadandó

 **Feladatleírás:** Feladatomban egy akváriumot szimulálok, amelyben található három vízinövény, illetve egy hal.

# Funkciók:

- Kamerakezelés
- Tér körbejárása
- 3D objektumok betöltése
- Textúrák
- Fényerő növelés, csökkentés
- Mozgás 
- Füst
- Áthelyezhető növények
- Használati útmutató a programban


## Használati útmutató

- Kamera forgatás
	Bal klikk + egér mozgatás
	
- Mozgatás
	W A S D billentyűk (jobbra, balra, előre, hátra)
	SPACE C billentyűk (fel, le)
	
- Objektumok mozgatása
	M billentyű megnyomásakor a hal előre "úszik"
	Az E billenyűt lenyomva tartva a W A S D billentyűkkel egyszerre, a növények áthelyezhetőek
	
- Fényerő növelés/csökkentés
	"+", "-" billentyűk
	
- Füst
	F gomb nyomvatartására "algásodás"
	F gomb felengedésére eltűnik
	
- HELP menü
	F1 billentyű
	
## Indítás: 

A Grafika_beadando mappában egy Make parancs kiadása után a beadando.exe fájl megnyitásával már meg is 
tekinthető a program.


