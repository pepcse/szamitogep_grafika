 
#include <stdio.h>  
#include <time.h> 
#include <unistd.h>
#include <math.h>
#include "model.h"
#include "callbacks.h"

#include "algae.h"

int usleep(int useconds);

GLfloat density = 0.0;

void create_fog(){

	for (int i = 0; i < 10; i++)
	{
		usleep(1000);
		if (density < 0.01) {
		density += 0.00001;
		GLfloat fogColor[4] = { 0.6, 1.0, 0.3, 1.0 };
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_EXP2);
		glFogfv(GL_FOG_COLOR, fogColor);
		glFogf(GL_FOG_DENSITY, density);
		glHint(GL_FOG_HINT, GL_NICEST);
		
	}
	
		
	}
	
}

void delete_fog() {
	GLfloat density = 0.0;
	GLfloat fogColor[4] = { 0.6, 1.0, 0.3, 1.0 };
	glEnable(GL_FOG);
	glFogi(GL_FOG_MODE, GL_EXP2);
	glFogfv(GL_FOG_COLOR, fogColor);
	glFogf(GL_FOG_DENSITY, density);
	glHint(GL_FOG_HINT, GL_NICEST);
}
